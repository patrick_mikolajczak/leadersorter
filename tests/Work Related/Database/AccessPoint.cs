using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace tests.Database
{
    [Table("AccessPoint")]
    public partial class AccessPoint
    {
        public AccessPoint()
        {
            TrackPoints = new HashSet<TrackPoint>();
        }

        public Guid AccessPointID { get; set; }

        public Guid? ProviderID { get; set; }

        public Guid? LocationID { get; set; }

        [Required]
        [StringLength(250)]
        public string Name { get; set; }

        [StringLength(500)]
        public string Description { get; set; }

        public decimal? Direction { get; set; }

        public decimal? Angle { get; set; }

        public decimal? Range { get; set; }

        public DateTime? ActiveDate { get; set; }

        public DateTime? InactiveDate { get; set; }

        public DateTime InsertDate { get; set; }

        public DateTime? LastSynchronized { get; set; }

        public int SynchronizationSourceID { get; set; }

        public virtual Location Location { get; set; }

        public virtual Provider Provider { get; set; }

        public virtual ICollection<TrackPoint> TrackPoints { get; set; }
    }
}
