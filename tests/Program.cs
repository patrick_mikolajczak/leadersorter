﻿using System;
using tests.FunProjects.ProducerConsumer;

namespace tests
{
    static class Program
    {
        public static void Main()
        {
            var test = new DataFlowProducerConsumer();
            test.RunTest();
            Console.ReadLine();
        }
    }
}
