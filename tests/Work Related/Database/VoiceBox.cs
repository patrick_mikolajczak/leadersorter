using System.Data.Entity;

namespace tests.Database
{
    public partial class VoiceBox : DbContext
    {
        public VoiceBox()
            : base("name=voicebox")
        {
        }

        public virtual DbSet<AccessPoint> AccessPoints { get; set; }
        public virtual DbSet<Line> Lines { get; set; }
        public virtual DbSet<Location> Locations { get; set; }
        public virtual DbSet<Provider> Providers { get; set; }
        public virtual DbSet<ProviderType> ProviderTypes { get; set; }
        public virtual DbSet<TrackPoint> TrackPoints { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Entity<AccessPoint>()
                .Property(e => e.Direction)
                .HasPrecision(5, 2);

            modelBuilder.Entity<AccessPoint>()
                .Property(e => e.Angle)
                .HasPrecision(5, 2);

            modelBuilder.Entity<AccessPoint>()
                .Property(e => e.Range)
                .HasPrecision(6, 2);

            modelBuilder.Entity<Line>()
                .Property(e => e.DefaultAssociateAreaCode)
                .IsUnicode(false);

            modelBuilder.Entity<Line>()
                .Property(e => e.DefaultTargetEquipmentID)
                .IsUnicode(false);

            modelBuilder.Entity<Line>()
                .Property(e => e.DefaultTargetSubscriberID)
                .IsUnicode(false);

            modelBuilder.Entity<Line>()
                .HasMany(e => e.TrackPoints)
                .WithRequired(e => e.Line)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<Location>()
                .Property(e => e.Longitude)
                .HasPrecision(16, 13);

            modelBuilder.Entity<Location>()
                .Property(e => e.Latitude)
                .HasPrecision(16, 13);

            modelBuilder.Entity<ProviderType>()
                .HasMany(e => e.Providers)
                .WithRequired(e => e.ProviderType)
                .WillCascadeOnDelete(false);

            modelBuilder.Entity<TrackPoint>()
                .Property(e => e.Longitude)
                .HasPrecision(16, 13);

            modelBuilder.Entity<TrackPoint>()
                .Property(e => e.Latitude)
                .HasPrecision(16, 13);
        }
    }
}
