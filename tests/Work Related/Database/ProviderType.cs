using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;

namespace tests.Database
{
    [Table("ProviderType")]
    public partial class ProviderType
    {
        public ProviderType()
        {
            Providers = new HashSet<Provider>();
        }

        [DatabaseGenerated(DatabaseGeneratedOption.None)]
        public int ProviderTypeID { get; set; }

        public virtual ICollection<Provider> Providers { get; set; }
    }
}
