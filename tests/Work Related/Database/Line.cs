using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace tests.Database
{
    public partial class Line
    {
        public Line()
        {
            TrackPoints = new HashSet<TrackPoint>();
        }

        public Guid LineID { get; set; }

        public Guid TargetID { get; set; }

        public int SourceTypeID { get; set; }

        [Required]
        [StringLength(50)]
        public string Name { get; set; }

        public DateTime Created { get; set; }

        public DateTime Updated { get; set; }

        public bool Enabled { get; set; }

        public bool DailyCounter { get; set; }

        public bool OffHookAlarm { get; set; }

        public bool AlarmedCallsOnly { get; set; }

        public bool AutoMinimize { get; set; }

        public short? AutoMinAfterSec { get; set; }

        public bool PensToArchive { get; set; }

        public bool UpdWorkingCopy { get; set; }

        public Guid? DefaultTeamID { get; set; }

        public Guid? CourtOrderID { get; set; }

        public Guid? RepositoryID { get; set; }

        public bool AllowStopRecording { get; set; }

        public int? DefaultLanguageID { get; set; }

        public Guid? DefaultTargetDNNodeID { get; set; }

        public bool IsFAXDecodeAuthorized { get; set; }

        public int ContentHandlingTypeID { get; set; }

        public int BeginLiveAudioMonitoringTriggerID { get; set; }

        public int RetainContentID { get; set; }

        public int ArchiveModeID { get; set; }

        public int TimeSourceID { get; set; }

        [StringLength(50)]
        public string InsertBy { get; set; }

        public DateTime? InsertDate { get; set; }

        [StringLength(50)]
        public string UpdateBy { get; set; }

        public DateTime? UpdateDate { get; set; }

        public DateTime? LastBackupDate { get; set; }

        public bool IsSMSDecodeAuthorized { get; set; }

        public bool IsModemDecodeAuthorized { get; set; }

        public int? ModemDecodeTypeID { get; set; }

        public int DefaultClassificationID { get; set; }

        public DateTime? LastAutomatedBackupDate { get; set; }

        public Guid? BackupJobID { get; set; }

        public bool FollowTargetInLAM { get; set; }

        public bool? BreakSessionsOnDurationLimit { get; set; }

        public int? SessionDurationLimit { get; set; }

        public bool? BreakSessionsOnContentFileSizeLimit { get; set; }

        public int? SessionContentFileSizeLimit { get; set; }

        public bool? AllowManualSessionBreak { get; set; }

        public bool CollectionValidationRequired { get; set; }

        public bool IsCollectionValid { get; set; }

        public bool AutomaticallyDeleteExpiredSessions { get; set; }

        public int DefaultRetainSessionID { get; set; }

        public int DefaultTimeZoneContextID { get; set; }

        public Guid? CollectionProfileID { get; set; }

        public int DefaultCollectionProductPresentationTimeZoneID { get; set; }

        public bool IsLocationMonitoringAuthorized { get; set; }

        public int OperationModeID { get; set; }

        public int EDSCollectionModeID { get; set; }

        public bool IsVideoAuthorized { get; set; }

        [StringLength(8)]
        public string DefaultAssociateAreaCode { get; set; }

        public int DefaultAssociateAreaCodeSettingID { get; set; }

        public bool CollectPostCutThruDigitsInSummaryMode { get; set; }

        public bool ConsensualIntercept { get; set; }

        public int RecordingTriggerForIncomingCallID { get; set; }

        public bool BreakSessionsOnTrackPointsLimit { get; set; }

        public int? SessionTrackpointsLimit { get; set; }

        [StringLength(50)]
        public string DefaultTargetEquipmentID { get; set; }

        public bool ApplyDefaultTargetEquipmentID { get; set; }

        [StringLength(50)]
        public string DefaultTargetSubscriberID { get; set; }

        public bool ApplyDefaultTargetSubscriberID { get; set; }

        public bool ApplyDefaultTargetDNID { get; set; }

        public bool IsIPDecodeAuthorized { get; set; }

        public Guid? DefaultExclusiveAccessGroupID { get; set; }

        public Guid? DefaultExclusiveAccessUserID { get; set; }

        public Guid? DefaultGroupAssignmentID { get; set; }

        [StringLength(30)]
        public string SourceID { get; set; }

        public bool OnlyPopulateValidatedDNs { get; set; }

        public bool ValidateAreaCodeExchangeForNorthAmericanNumbers { get; set; }

        public bool FormatValidatedDNs { get; set; }

        public bool IsSMSLiveMonitoringRequired { get; set; }

        public bool ShowSMSContentInLAM { get; set; }

        public int DefaultSMSClassificationID { get; set; }

        public bool IsPhoneticIndexAutomatic { get; set; }

        public bool PutAway { get; set; }

        public bool PutAwayRecording { get; set; }

        public bool IsRecordingEvidenceDirectToMedia { get; set; }

        [StringLength(32)]
        public string RecordEvidenceDirectToMediaVolumeName { get; set; }

        public bool AutomatedLineBackup { get; set; }

        public bool CollectPostCutThruDigitsInContentMode { get; set; }

        public bool AcceptLiveLIIDUpdates { get; set; }

        public bool BreakSessionsOnMessagesLimit { get; set; }

        public int SessionMessagesLimit { get; set; }

        public bool BreakSessionsOnTimeBetweenMessagesLimit { get; set; }

        public int SessionTimeBetweenMessagesLimit { get; set; }

        public virtual ICollection<TrackPoint> TrackPoints { get; set; }
    }
}
