﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using tests.Database;

namespace tests
{
    class SoaringTestDataGenerator
    {
        internal struct Point
        {
            public decimal? Latitude;
            public decimal? Longitude;
        }

        internal enum Direction
        {
            North,
            Southeast,
            Southwest

        }

        public static Point GeneratePoint()
        {
            var rand = new Random((int)DateTime.Now.Ticks);
            var point = new Point { Latitude = rand.Next(-90, 90), Longitude = rand.Next(-180, 180) };
            return CleanPoint(point);
        }

        public static Point GenerateLocation(Direction direction, decimal? lat, decimal? lng)
        {
            var rand = new Random((int)DateTime.Now.Ticks);
            decimal? latDelta;
            decimal? longDelta;
            Point point;
            switch (direction)
            {
                case Direction.North:
                    latDelta = rand.Next(0, 1000 / 100000);
                    longDelta = rand.Next(0, 1000) / 100000;
                    point = new Point { Latitude = lat + latDelta, Longitude = lng + longDelta };
                    return CleanPoint(point);
                case Direction.Southeast:
                    latDelta = rand.Next(0, 1000 / 100000);
                    longDelta = rand.Next(0, 1000) / 100000;
                    point = new Point { Latitude = lat - latDelta, Longitude = lng - longDelta };
                    return CleanPoint(point);
                case Direction.Southwest:
                    latDelta = rand.Next(0, 1000 / 100000);
                    longDelta = rand.Next(0, 1000) / 100000;
                    point = new Point { Latitude = lat - latDelta, Longitude = lng + longDelta };
                    return CleanPoint(point);
                default:
                    throw new ArgumentOutOfRangeException("direction");
            }
        }

        private static Point CleanPoint(Point point)
        {
            if (point.Latitude > 90) point.Latitude = 90;
            if (point.Latitude < -90) point.Latitude = -90;
            if (point.Latitude > 180) point.Longitude = 180;
            if (point.Latitude < -180) point.Longitude = -180;
            return point;
        }

        private static void GenerateLocations(VoiceBox voiceBox)
        {
            for (var i = 0; i < 500; i++)
            {
                var point = GeneratePoint();
                var location = new Location { Longitude = point.Longitude, Latitude = point.Latitude, LocationTypeID = 1, Description = "Description" };
                voiceBox.Locations.Add(location);
            }
            voiceBox.SaveChanges();

        }

        private static void GenerateProviders(VoiceBox voiceBox)
        {
            var providerType = voiceBox.ProviderTypes.First();
            var provider1 = new Provider { Name = "Rogers", ProviderID = Guid.NewGuid(), ProviderType = providerType };
            var provider2 = new Provider { Name = "Bell", ProviderID = Guid.NewGuid(), ProviderType = providerType };

            voiceBox.Providers.Add(provider1);
            voiceBox.Providers.Add(provider2);
            voiceBox.SaveChanges();
        }

        public void FillData()
        {
            var voiceBox = new VoiceBox();
            GenerateProviders(voiceBox);
            GenerateLocations(voiceBox);
            GenerateAccessPoints(voiceBox);
            GenerateTrackPoints(voiceBox);
        }

        private void GenerateTrackPoints(VoiceBox voiceBox)
        {
            var rand = new Random((int)DateTime.Now.Ticks);
            var line = voiceBox.Lines.First();
            foreach (var location in voiceBox.Locations)
            {

                if (rand.Next(0, 100) > 65)
                {
                    GenerateTrackPointWithAccessPoint(voiceBox, location);
                }
                else
                {
                    GenerateSoloTrackPoint();
                }

            }
            voiceBox.SaveChanges();
        }

        private void GenerateSoloTrackPoint()
        {
            var trackpoint = new TrackPoint();
            var point = GeneratePoint();
            trackpoint.Longitude = point.Longitude;
            trackpoint.Latitude = point.Latitude;
        }

        private void GenerateTrackPointWithAccessPoint(VoiceBox voiceBox, Location location)
        {
            for (var i = 0; i < location.AccessPoints.Count; i++)
            {
                var trackpoint = new TrackPoint();
                Point point;
                switch (i)
                {
                    case 0:
                        point = GenerateLocation(Direction.North, location.Latitude, location.Longitude);
                        trackpoint.Longitude = point.Longitude;
                        trackpoint.Latitude = point.Latitude;
                        break;
                    case 1:
                        point = GenerateLocation(Direction.Southeast, location.Latitude, location.Longitude);
                        trackpoint.Longitude = point.Longitude;
                        trackpoint.Latitude = point.Latitude;
                        break;
                    case 2:
                        point = GenerateLocation(Direction.Southwest, location.Latitude, location.Longitude);
                        trackpoint.Longitude = point.Longitude;
                        trackpoint.Latitude = point.Latitude;
                        break;
                    default:
                        point = GenerateLocation(Direction.North, location.Latitude, location.Longitude);
                        trackpoint.Longitude = point.Longitude;
                        trackpoint.Latitude = point.Latitude;
                        break;
                }
                voiceBox.TrackPoints.Add(trackpoint);
            }
            voiceBox.SaveChanges();
        }


        /// <summary>
        /// Attach Access Point configurations to each location made.
        /// </summary>
        /// <param name="voiceBox"></param>
        private void GenerateAccessPoints(VoiceBox voiceBox)
        {
            var rand = new Random((int)DateTime.Now.Ticks);
            foreach (var location in voiceBox.Locations)
            {
                switch (rand.Next(1, 2))
                {
                    case 1:
                        voiceBox.AccessPoints.Add(GenerateAccessPoint(location, 120, 1, 0));
                        voiceBox.AccessPoints.Add(GenerateAccessPoint(location, 120, 1, 120));
                        voiceBox.AccessPoints.Add(GenerateAccessPoint(location, 120, 1, -120));
                        break;
                    default:
                        voiceBox.AccessPoints.Add(GenerateAccessPoint(location, 360, 1, 0));
                        break;
                }
            }
            voiceBox.SaveChanges();
        }

        private AccessPoint GenerateAccessPoint(Location location, decimal? angle, decimal? range, decimal? direction)
        {
            var ap = new AccessPoint
            {
                AccessPointID = Guid.NewGuid(),
                Provider = GetRandomProvider(),
                Name = String.Format("AccessPoint - {0}", 1),
                Location = location,
                Angle = angle,
                Range = range,
                Direction = direction
            };
            return ap;
        }


        private Provider GetRandomProvider()
        {
            return new Provider();
        }
    }
}
