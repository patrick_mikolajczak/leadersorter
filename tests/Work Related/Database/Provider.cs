using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace tests.Database
{
    [Table("Provider")]
    public partial class Provider
    {
        public Provider()
        {
            AccessPoints = new HashSet<AccessPoint>();
        }

        public Guid ProviderID { get; set; }

        [Required]
        [StringLength(250)]
        public string Name { get; set; }

        public int ProviderTypeID { get; set; }

        public virtual ICollection<AccessPoint> AccessPoints { get; set; }

        public virtual ProviderType ProviderType { get; set; }
    }
}
