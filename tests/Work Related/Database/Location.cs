using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace tests.Database
{
    [Table("Location")]
    public partial class Location
    {
        public Location()
        {
            AccessPoints = new HashSet<AccessPoint>();
        }

        public Guid LocationID { get; set; }

        public int LocationTypeID { get; set; }

        [StringLength(500)]
        public string Description { get; set; }

        public decimal? Longitude { get; set; }

        public decimal? Latitude { get; set; }

        [StringLength(50)]
        public string AddressLine1 { get; set; }

        [StringLength(50)]
        public string AddressLine2 { get; set; }

        [StringLength(50)]
        public string City { get; set; }

        [StringLength(25)]
        public string Province { get; set; }

        [StringLength(50)]
        public string Country { get; set; }

        [StringLength(11)]
        public string PostalZipCode { get; set; }

        public virtual ICollection<AccessPoint> AccessPoints { get; set; }
    }
}
