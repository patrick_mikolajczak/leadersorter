﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;

namespace tests.Extensions
{
    public class TestHarness
    {

        public static void TimeStatistic(string message, int numberOfRuns, Action code)
        {
            var results = new List<TimeSpan>();
            //Create threads equal to the # of cores;
            for (var i = 0; i < numberOfRuns; i++)
            {
                results.Add(TimeTrial(message, code));
            }

            OutputResults(results, message);

        }

        private static void OutputResults(List<TimeSpan> results, string message)
        {
            var low = results.Min();
            var high = results.Max();
            var average = results.Average(x => x.Ticks);

            Console.WriteLine("Overall: Min: {0:0.00} Ticks\t Max: {1:0.00} Ticks\t Average {2:0.00} Ticks \t - {3:0.00}", low.Ticks, high.Ticks, average, message);
        }

        public static T TimeStatistic<T>(string message, int numberOfRuns, Func<T> code)
        {
            var results = new List<TimeSpan>();
            var result = default(T);
            //Create threads equal to the # of cores;
            for (var i = 0; i < numberOfRuns; i++)
            {
                var x = TimeTrial(message, code);
                results.Add(x.Item1);
                result = x.Item2;
            }

            OutputResults(results, message);
            return result;
        }

        public static Tuple<TimeSpan, T> TimeTrial<T>(string message, int interations, Func<T> code)
        {
            return TimeTrial(message, code);
        }

        public static Tuple<TimeSpan, T> TimeTrial<T>(string message, Func<T> code)
        {
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            var returnVal = code();
            stopwatch.Stop();
            var timediff = stopwatch.Elapsed;
            // Console.WriteLine("{0}: {1}ms", message, timediff.TotalMilliseconds);
            return new Tuple<TimeSpan, T>(timediff, returnVal);
        }

        public static TimeSpan TimeTrial(string message, Action code)
        {
            var stopwatch = new Stopwatch();
            stopwatch.Start();
            code();
            stopwatch.Stop();
            var timediff = stopwatch.Elapsed;
            // Console.WriteLine("{0}: {1}ms", message, timediff.TotalMilliseconds);
            return timediff;
        }
    }
}
