using System;
using System.ComponentModel.DataAnnotations.Schema;

namespace tests.Database
{
    [Table("TrackPoint")]
    public partial class TrackPoint
    {
        public Guid TrackPointID { get; set; }

        public Guid LineID { get; set; }

        public int? ObjectTypeID { get; set; }

        public Guid? ObjectID { get; set; }

        public Guid? AccessPointID { get; set; }

        public DateTime StartOfCollection { get; set; }

        public DateTime? EndOfCollection { get; set; }

        public int AccuracyTypeID { get; set; }

        public string SourceLocationData { get; set; }

        public decimal? Longitude { get; set; }

        public decimal? Latitude { get; set; }

        public int? DatumID { get; set; }

        public int? TimeSourceID { get; set; }

        public float? VelocityInMetersPerSecond { get; set; }

        public float? HeadingInDegrees { get; set; }

        public float? ElevationInMeters { get; set; }

        public int ConfidenceID { get; set; }

        public virtual AccessPoint AccessPoint { get; set; }

        public virtual Line Line { get; set; }
    }
}
